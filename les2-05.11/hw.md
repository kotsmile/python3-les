# Домашнее задание №2. Строка. Работа со строками. Методы строк.

1. На вход вводится имя `name`. Вывести фразу `Hello, name!`.
2. На вход вводится названия фрукта `name` и его количесвто `n`. Вывести это фрукт ровно `n` раз и каждый на новой строчке.
3. На вход вводится число `n`. Вывести `Robot_v_n`.
4. На вход вводится строка `s1` и строка `s2`. Вывести первую букву первой строки и послкднюю букву второй строки, разделив пробелом.
5. Вводится зашифрованное сообщение `encode_msg`. Чтобы его расшифровать необходимо взять каждую вторую букву сообщения начиная с первого символа. Расшифруйте сообщение.
6. Вводиться зашифрованное сообщение `encode_msg`. Чтобы его расшифровать необхадимо взять каждый третий символ, начиная с последнего элемента и заканчивая первым. Расшифруйте сообщение.
